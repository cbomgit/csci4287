#include<math.h>
#include<SoftwareSerial.h>
SoftwareSerial myConnection(0, 1);


int bottomOutsidePin =7;
int bottomInsidePin = 6;
int topInsidePin = 5;
int topOutsidePin = 4;
int leftOutsidePin = 3;
int leftInsidePin = 2;
int rightInsidePin = 1;
int rightOutsidePin = 0;
int BO = 0;
int BI = 0;
int TI = 0;
int TO = 0;
int LO = 0;
int LI = 0;
int RI = 0;
int RO = 0;
int count = 0;
double leftEdgeAngle = 0.00, rightEdgeAngle = 0.00,  middleAngle = 0.00,
topEdgeAngle =0.0, bottomEdgeAngle = 0.0, middleVAngle = 0.0;
bool inScreen = false;

void setup() {
  
  myConnection.begin(9600);
}

void loop() {

    BO = analogRead(7);
    BI = analogRead(6);
    TI = analogRead(5);
    TO = analogRead(4);  
    LO = analogRead(3);
    LI = analogRead(2);
    RI = analogRead(1);
    RO = analogRead(0);
    
    //myConnection.println(TO);
    //myConnection.println(TI);
    middleAngle    = getAngle(LI, RI);
    leftEdgeAngle  = getAngle(LO, LI);
    rightEdgeAngle = getAngle(RI, RO);
    middleVAngle = getAngle(TI, BI);
    topEdgeAngle = getAngle(TO, TI);
    bottomEdgeAngle = getAngle(BI, BO);
    myConnection.println(middleAngle);
    myConnection.println(middleVAngle);
    myConnection.println(topEdgeAngle);
    myConnection.println(bottomEdgeAngle);
    myConnection.println(rightEdgeAngle);
    //myConnection.println(middleAngle);
    myConnection.println(leftEdgeAngle);
    
    if ((middleVAngle > -1000 && leftEdgeAngle != -1000 && rightEdgeAngle != -1000)|| middleAngle > -1000){
      inScreen = true;
      count = 0;
    } else if(middleAngle == -1000 || middleVAngle == -1000){
      count++;
      if(count >= 2){
        inScreen = false;
      }
    }else {inScreen = false;}
 
    //write inScreen variable to bluetooth in this location.
    if(inScreen) {
      myConnection.println("true");
    }
    else {
      myConnection.println("false");
    }
  delay(100);
}


double toRadians(double degrees) {
    double radians = degrees * pi() / 180;
    return radians;
}

float pi() {
  return (atan(1) * 4);
}

double getAngle(int leftPinCurr, int rightPinCurr) {

  int adc1 = leftPinCurr;
  int adc2 = rightPinCurr;
  unsigned int noise1 = 202;
  unsigned int noise2 = 202;

                                     // ANGLE COMPUTATION                    
        int ir_angle = -1000;
        double ir_angle_abs = -1000;
        double temp = -1000;
        
        double thresh = 15;// 25 for 80deg PDs, 45 for 40deg PDs                
        double gamma = 11.1434;// 2.6 for 80deg PDs, 11.1434 for 40degPDs
        double halfang = 20; // 40 for 80 deg PDs
        double halfangvert = 24; // 40 for 80 deg PDs
        // angle is in degrees and stored in ir_angle variable


  if(adc1 < 0 || adc1 > 1023) {
    adc1 = 0;
  }

  if(adc2 < 0 || adc2 > 1023) {
    adc2 = 0;
  }


        if(noise1 <= adc1 && noise2 <= adc2){
    
        // computing vertical angle******
        //temp_vert = pow((double)adc6,(double)1/gamma)/(pow((double)adc1,(double)1/gamma) + pow((double)adc2,(double)1/gamma));
  //temp_vert = pow((double)adc6,(double)1/gamma)/(pow((double)1200,(double)1/gamma) + pow((double)1200,(double)1/gamma));
                                                     
        //  ir_angle_vert = (int)(180/3.14)*atan((1/tan(toRadians(halfangvert)) - (2*temp_vert*(1/sin(toRadians(halfangvert))))));
        //*******************************
 
          if (adc1-noise1>= thresh && adc2-noise2>= thresh && adc1-noise1>= adc2-noise2) {
            /*temp = (double)pow((double) ((double)(adc1-noise1) / (double)(adc2-noise2)), (double)1/11.1434);
            ir_angle_abs = -(180 / 3.14) * atan((1 / tan(toRadians(20.0))) * ((temp - 1) / (temp + 1)));*/
      temp = (double)pow((double) ((double)(adc1-noise1) / (double)(adc2-noise2)), (double)1/gamma);
            ir_angle_abs = -(180 / 3.14) * atan((1 / tan(toRadians(halfang))) * ((temp - 1) / (temp + 1)));
            ir_angle = (int) ir_angle_abs;
        
                }
                else if (adc1-noise1>=thresh && adc2-noise2>= thresh && adc1-noise1< adc2-noise2) {
                        temp = (double)pow((double) ((double)(adc2-noise2) / (double)(adc1-noise1)), (double)1/gamma);
                        ir_angle_abs = -(180 / 3.14) * atan((1 / tan(toRadians(halfang))) * (temp - 1) / (temp + 1));
                        ir_angle = -(int) ir_angle_abs;
                }
                else{
                    if(adc1-noise1 > adc2-noise2 && adc1-noise1 >= 10){
                        ir_angle = -100;
                        ir_angle_abs = -100;
                    }else if(adc1-noise1 < adc2-noise2 && adc2-noise2 >= 10){
                        ir_angle = 100;
                        ir_angle_abs = 100;
                    }
                }
       }
   
       else{
           ir_angle = -1000;
           ir_angle_abs=-1000; // */
           /* if(adc1-noise1 > adc2-noise2){
           ir_angle = -100;
           ir_angle_abs = -100;
           }else{
                                                ir_angle = 100;
                                                ir_angle_abs = 100;
                                             }*/
       }


  return ir_angle_abs;
}
