#include <SoftwareSerial.h>
SoftwareSerial myConnection (0, 1);

int pressureSensorPin = A1;
float threshold = .6;

void setup() {


myConnection.begin(9600);


}

void loop() {
  
  int pressureValue = analogRead (pressureSensorPin);
  float pressureVoltage = pressureValue * (5.0 / 1023.0);
  if (pressureVoltage > threshold)
  myConnection.println("true");
  
  delay (100);
  
}