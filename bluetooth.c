#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include "bluetooth.h"

int  configurePins() {
	
	static const char* portName = "/dev/ttyS1";

	char path[256];
	char buffer[64];
	int i;	
	for(i = 0; i <=1; i++) {
		memset(path, 0, sizeof(path));
		sprintf(path, "%s%s%d", GPIO_MODE_PATH, GPIO_FILENAME, i);

		int pinMode = open(path, O_RDWR);

		if(pinMode == -1) {
			fprintf(stderr, "Could not open pin for reading\n");
		}

		setPinMode(pinMode, SERIAL);
		close(pinMode);
	}
	
	int serialPort; // File descriptor for serial port
  	struct termios portOptions; // struct to hold the port settings
  	// Open the serial port as read/write, not as controlling terminal, and
  	//   don't block the CPU if it takes too long to open the port.
  	serialPort = open(portName, O_RDWR | O_NOCTTY | O_NDELAY );
	if(serialPort == -1) {
		fprintf(stderr, "Could not open serial port.\n");
		exit(EXIT_FAILURE);
	}

  	// Fetch the current port settings
  	tcgetattr(serialPort, &portOptions);

  	// Flush the port's buffers (in and out) before we start using it
  	tcflush(serialPort, TCIOFLUSH);

  	// Set the input and output baud rates
  	cfsetispeed(&portOptions, B115200);
  	cfsetospeed(&portOptions, B115200);

  	// c_cflag contains a few important things- CLOCAL and CREAD, to prevent
  	//   this program from "owning" the port and to enable receipt of data.
  	//   Also, it holds the settings for number of data bits, parity, stop bits,
  	//   and hardware flow control. 
  	portOptions.c_cflag |= CLOCAL;
  	portOptions.c_cflag |= CREAD;
  	// Set up the frame information.
  	portOptions.c_cflag &= ~CSIZE; // clear frame size info
  	portOptions.c_cflag |= CS8;    // 8 bit frames
  	portOptions.c_cflag &= ~PARENB;// no parity
  	portOptions.c_cflag &= ~CSTOPB;// one stop bit

  	// Now that we've populated our options structure, let's push it back to the
  	//   system.
  	tcsetattr(serialPort, TCSANOW, &portOptions);

  	// Flush the buffer one more time.
  	tcflush(serialPort, TCIOFLUSH);

	return serialPort;

}

void sendToBluetooth(int serialPort, char *msg, int msgLength) {
	int i;
  	i = write(serialPort, msg, msgLength);
	//printf("Sent %s. %d\n bytes out of %d.", msg, i, msgLength);
	sleep(.5);
	
}


void setPinMode(int pinID, int mode)
{
  writeFile(pinID, mode);
}

// While it seems okay to only *read* the first value from the file, you
//   seemingly must write four bytes to the file to get the I/O setting to
//   work properly. This function does that.
void writeFile(int fileID, int value)
{
  char buffer[4];  // A place to build our four-byte string.
  memset((void *)buffer, 0, sizeof(buffer)); // clear the buffer out.
  sprintf(buffer, "%d", value);
  lseek(fileID, 0, SEEK_SET);   // Make sure we're at the top of the file!
  int res = write(fileID, buffer, sizeof(buffer));
}
