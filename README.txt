README.txt


SET UP: 

1. Connect analog pins to Pin 1 of each op-amp and to analog pins 0 - 7 of the Arduino. 

2. Power and ground from each photodiode pair should go to one common ground/power and then to the pcDuino. 
   We used another breadboard for this.

3. Connect TX,RX from Bluetooth to RX/TX of Mega. Connect Power and ground from bluetooth to common power and ground.

4. Connect Pressure sensor to Arduino. Connect ground and an ADC.

5. Power on both Arduinos. Pair phone to both bluetooths. 

6. Power on or attach a battery to the receiver.

7. System is ready to use.


