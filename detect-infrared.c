#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include "analog.h"
#include <math.h>
#include <sys/types.h>
#include "bluetooth.h"

#define THRESHOLD 10

double toRadians(double degrees);
float pi();
int analogRead(int pin);
double getAngle(int leftPin, int rightPin);
void init();


int main(void)
{

	int pinCurrent;

	//variabes used to identify pins
	int leftOutside = 2, leftInside = 3;
	int rightInside = 4, rightOutside = 5;
		
	//variables used to store read current at the adc pins
	int leftOutsideCurrent, leftInsideCurrent; 
	int rightInsideCurrent, rightOutsideCurrent;
	double leftEdgeAngle = 0.00, rightEdgeAngle = 0.00,  middleAngle = 0.00;
	
	//variales to decide if looking past the edge of the screen
	int count = 0;
	int inScreen = 0;

	int serialPort = configurePins();

	while(1) {
		//read the current of the left pair
		leftOutsideCurrent = analogRead(leftOutside);
		leftInsideCurrent  = analogRead(leftInside);

		rightInsideCurrent = analogRead(rightInside);
		rightOutsideCurrent = analogRead(rightOutside);
		middleAngle    = getAngle(leftInsideCurrent, rightInsideCurrent);
		leftEdgeAngle  = getAngle(leftOutsideCurrent, leftInsideCurrent);
		rightEdgeAngle = getAngle(rightInsideCurrent, rightOutsideCurrent);	
		printf("Left: %.2f\tMiddle :%.2f\tRight: %.2f\n",leftEdgeAngle, middleAngle, rightEdgeAngle);
		
		if(middleAngle == -1000){
			count++;
			if(count >= 2){
				inScreen = 0;
			}
		}else if((leftEdgeAngle > -1000 && middleAngle > -1000) || (middleAngle > -1000 && rightEdgeAngle > -1000)){
			inScreen = 1;
			count = 0;
		}
		//write inScreen variable to bluetooth in this location.

		if(inScreen) {
			sendToBluetooth(serialPort, "true", 5);
			printf("Looking at screen. Sending Thumbs up.\n");
		}
		else {
			sendToBluetooth(serialPort, "false", 6);
			printf("Not looking at screen. Sending Thumbs down.\n");
		}
	
		usleep(1000000);
	}	
	return 0;
}

int analogRead(int pin) {

	char ADCBuffer[16];
	int res, pinCurrent;	
	int fileDesc;
	char path[64];
	
	memset(path, 0, sizeof(path));
	sprintf(path, "%s%s%d", ADC_IF_PATH, ADC_IF_FILE, pin);
	fileDesc = open(path, O_RDONLY);
	
	lseek(fileDesc, 0, SEEK_SET);
	res = read(fileDesc, ADCBuffer, sizeof(ADCBuffer));
	pinCurrent = atoi(ADCBuffer+5);
 	usleep(10000);	
	
	lseek(fileDesc, 0, SEEK_SET);
	res = read(fileDesc, ADCBuffer, sizeof(ADCBuffer));
	pinCurrent = atoi(ADCBuffer+5);
	close(fileDesc);
 	usleep(10000);	
	return pinCurrent;

}


double toRadians(double degrees) {
    double radians = degrees * pi() / 180;
    return radians;
}

double getAngle(int leftPinCurr, int rightPinCurr) {

	int adc1 = leftPinCurr;
	int adc2 = rightPinCurr;
	unsigned int noise1 = 1550;
	unsigned int noise2 = 1550;

	                                   // ANGLE COMPUTATION                    
        int ir_angle = -1000;
        double ir_angle_abs = -1000;
        double temp = -1000;
        
        double thresh = 45;// 25 for 80deg PDs, 45 for 40deg PDs                
        double gamma = 11.1434;// 2.6 for 80deg PDs, 11.1434 for 40degPDs
        double halfang = 20; // 40 for 80 deg PDs
        double halfangvert = 24; // 40 for 80 deg PDs
        // angle is in degrees and stored in ir_angle variable


	if(adc1 < 0 || adc1 > 4095) {
		adc1 = 0;
	}

	if(adc2 < 0 || adc2 > 4095) {
		adc2 = 0;
	}


        if(noise1 <= adc1 && noise2 <= adc2){
		
        // computing vertical angle******
        //temp_vert = pow((double)adc6,(double)1/gamma)/(pow((double)adc1,(double)1/gamma) + pow((double)adc2,(double)1/gamma));
	//temp_vert = pow((double)adc6,(double)1/gamma)/(pow((double)1200,(double)1/gamma) + pow((double)1200,(double)1/gamma));
                                                     
        //  ir_angle_vert = (int)(180/3.14)*atan((1/tan(toRadians(halfangvert)) - (2*temp_vert*(1/sin(toRadians(halfangvert))))));
        //*******************************
 
        	if (adc1-noise1>= thresh && adc2-noise2>= thresh && adc1-noise1>= adc2-noise2) {
        		/*temp = (double)pow((double) ((double)(adc1-noise1) / (double)(adc2-noise2)), (double)1/11.1434);
        		ir_angle_abs = -(180 / 3.14) * atan((1 / tan(toRadians(20.0))) * ((temp - 1) / (temp + 1)));*/
			temp = (double)pow((double) ((double)(adc1-noise1) / (double)(adc2-noise2)), (double)1/gamma);
        		ir_angle_abs = -(180 / 3.14) * atan((1 / tan(toRadians(halfang))) * ((temp - 1) / (temp + 1)));
        		ir_angle = (int) ir_angle_abs;
        
                }
                else if (adc1-noise1>=thresh && adc2-noise2>= thresh && adc1-noise1< adc2-noise2) {
                        temp = (double)pow((double) ((double)(adc2-noise2) / (double)(adc1-noise1)), (double)1/gamma);
                        ir_angle_abs = -(180 / 3.14) * atan((1 / tan(toRadians(halfang))) * (temp - 1) / (temp + 1));
                        ir_angle = -(int) ir_angle_abs;
                }
                else{
                    if(adc1-noise1 > adc2-noise2 && adc1-noise1 >= 10){
                        ir_angle = -100;
                        ir_angle_abs = -100;
                    }else if(adc1-noise1 < adc2-noise2 && adc2-noise2 >= 10){
                        ir_angle = 100;
                        ir_angle_abs = 100;
                    }
                }
       }
   
       else{
           ir_angle = -1000;
           ir_angle_abs=-1000; // */
           /* if(adc1-noise1 > adc2-noise2){
           ir_angle = -100;
           ir_angle_abs = -100;
           }else{
                                                ir_angle = 100;
                                                ir_angle_abs = 100;
                                             }*/
       }


	return ir_angle_abs;
}


float pi() {
	return (atan(1) * 4);
}
