#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/rfcomm.h>
#include <sys/types.h>
#include <fcntl.h>
#include "rfcomm.h"

    struct sockaddr_rc addr = { 0 };
    int s, status;
    char dest[18] = "20:13:11:26:07:52";   //MAC Address of HC-05
    char voltage[4];
    int bytesRead = 0;

void connect_foot_pedal()
{
     // allocate a socket
        s = socket(AF_BLUETOOTH, SOCK_STREAM, BTPROTO_RFCOMM);
	if (s<0)
	error("Error opening socket");

    // set the connection parameters (who to connect to)
    addr.rc_family = AF_BLUETOOTH;
    addr.rc_channel = (uint8_t) 1;
    str2ba( dest, &addr.rc_bdaddr );

    // connect to server
    status = connect(s, (struct sockaddr *)&addr, sizeof(addr));
    if (status <0)
	error("Error connecting");
}



char *get_voltage_pedal()
{

bytesRead = read(s, voltage, sizeof( char));


return voltage;


}
